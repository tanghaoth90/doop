package org.clyze.deepdoop.datalog.expr

import org.clyze.deepdoop.actions.IVisitable

interface IExpr extends IVisitable {}
