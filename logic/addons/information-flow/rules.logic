#include "macros.logic"

AnyMethodInvocation(?invocation, ?tomethod) <-
   StaticMethodInvocation(?invocation, ?tomethod, _).

AnyMethodInvocation(?invocation, ?tomethod) <-
   VirtualMethodInvocation(?invocation, ?tomethod, _).

AnyMethodInvocation(?invocation, ?tomethod) <-
   MethodInvocation:Method[?invocation] = ?tomethod,
   SpecialMethodInvocation:Insn(?invocation).

AnyMethodInvocationOpt(?tomethod, ?invocation) <- AnyMethodInvocation(?invocation, ?tomethod).

VarUse(?from, ?insn) <- AssignOper:From(?insn, ?from).

TaintedVarTaintedFromVar(?toCtx, ?to, ?value, ?type) <-
   VarIsTaintedFromVar(?type, ?toCtx, ?to, ?fromCtx, ?from),
   TaintedVarPointsTo(?value, ?fromCtx, ?from).

TaintedValueTransferred(?value, ?type, DEFAULT_BREADCRUMB, ?newvalue),
VarPointsTo(?hctx, ?newvalue, ?toCtx, ?to) <-
   TaintedVarTaintedFromVar(?toCtx, ?to, ?value, ?type),
   ImmutableHContext(?hctx).

TaintedValueTransferred(?value, ?type, DEFAULT_BREADCRUMB, ?newvalue),
VarPointsTo(?hctx, ?newvalue, ?toCtx, ?to) <-
   VarIsTaintedFromValue(?type, ?value, ?toCtx, ?to),
   ImmutableHContext(?hctx).
   

CallTaintingMethod(?label, ?ctx, ?invocation) <-
   TaintSourceMethod(?label, ?tomethod),
   Instruction:Method[?invocation] = ?inmethod,
   ApplicationMethod(?inmethod),
   MethodInvocationInContext(?ctx, ?invocation, ?tomethod).

/**
 * Information flow through complex relations
 */
StringFactoryType(?type) <-
  Type:Id(?type:"java.lang.String") ;
  Type:Id(?type:"java.lang.StringBuffer") ;
  Type:Id(?type:"java.lang.StringBuilder").

StringFactoryVar(?var) <-
  Var:Type[?var] = ?type,
  StringFactoryType(?type).

StringFactoryVarPointsTo(?factoryHctx, ?factoryValue, ?ctx, ?var) <-
  VarPointsTo(?factoryHctx, ?factoryValue, ?ctx, ?var),
  StringFactoryVar(?var).

Iaminterested6(?type, ?ctx, ?ret, ?ctx, ?param),
VarIsTaintedFromVar(?type, ?ctx, ?ret, ?ctx, ?base),
VarIsTaintedFromVar(?type, ?ctx, ?ret, ?ctx, ?param) <-
  Var:Type[?ret] = ?type,
  VirtualMethodInvocation:SimpleName[?invocation] = "append",
  VirtualMethodInvocation:Base[?invocation] = ?base,
  StringFactoryVarPointsTo(_, _, ?ctx, ?base),
  ActualParam[0, ?invocation] = ?param,
  AssignReturnValue[?invocation] = ?ret.

Iaminterested10(?type, ?ctx, ?base, ?ctx, ?param),
VarIsTaintedFromVar(?type, ?ctx, ?base, ?ctx, ?param) <-
  Var:Type[?base] = ?type,
  VirtualMethodInvocation:SimpleName[?invocation] = "append",
  VirtualMethodInvocation:Base[?invocation] = ?base,
  StringFactoryVarPointsTo(_, _, ?ctx, ?base),
  ActualParam[0, ?invocation] = ?param.


MethodReturningStringFactory(?tomethod) <-
  Method:ReturnType[?tomethod] = ?stringFactoryType,
  StringFactoryType(?stringFactoryType).

VirtualMethodInvocationOpt(?invocation, ?tomethod) <-
   VirtualMethodInvocation(?invocation, ?tomethod, _).

StringFactoryReturnInvocation(?invocation) <-
   VirtualMethodInvocationOpt(?invocation, ?tomethod),
   MethodReturningStringFactory(?tomethod).

VarIsTaintedFromVar(?type, ?ctx, ?var, ?ctx, ?var) -> Type(?type), Context(?ctx), Var(?var).

Iaminterested8(?type, ?ctx, ?ret, ?ctx, ?base),
VarIsTaintedFromVar(?type, ?ctx, ?ret, ?ctx, ?base) <-
  Var:Type[?ret] = ?type,
  StringFactoryReturnInvocation(?invocation),
  VirtualMethodInvocation:Base[?invocation] = ?base,
  StringFactoryVarPointsTo(_, _, ?ctx, ?base),
  AssignReturnValue[?invocation] = ?ret.

VarIsTaintedFromValue(?type, ?value, ?ctx, ?var) -> Type(?type), Value(?value), Context(?ctx), Var(?var).

Iaminterested(?componentType, ?basevalue, ?ctx, ?to),
VarIsTaintedFromValue(?componentType, ?basevalue, ?ctx, ?to) <-
   LoadHeapArrayIndex(?ctx, ?to, _, ?basevalue),
   Value:Type[?basevalue] = ?arrayType,
   ComponentType[?arrayType] = ?componentType,
   TaintedValue(?basevalue).


Iaminterested7(?type, ?ctx, ?ret, ?ctx, ?base),
VarIsTaintedFromVar(?type, ?ctx, ?ret, ?ctx, ?base) <-
   MethodInvocationInContext(?ctx, ?invocation, ?method),
   BaseToRetTaintTransferMethod(?method),
   MethodInvocation:Base[?invocation] = ?base,
   TypeForReturnValue(?type, ?ret, ?invocation).


MethodInvocationInfo(?invocation, ?type, ?ret) <-
  AnyMethodInvocationOpt(?method, ?invocation),
  Method:ReturnType[?method] = ?type,
  AssignReturnValue[?invocation] = ?ret.

VarIsCast(?var) <- AssignCast(_, ?var, _, _).

TypeForReturnValue(?type, ?ret, ?invocation) <-
   MethodInvocationInfo(?invocation, _, ?ret),
   OptAssignCast(?type, _, ?ret).

TypeForReturnValue(?type, ?ret, ?invocation) <-
   MethodInvocationInfo(?invocation, ?type, ?ret),
   !VarIsCast(?ret).

// Taint transfer through aliasing

MethodInvocation:Base[?invocation] = ?base <-
  VirtualMethodInvocation:Base[?invocation] = ?base ;
  SpecialMethodInvocation:Base[?invocation] = ?base.

TaintTransferMethodInvocationInContext(?ctx, ?index, ?invocation) <-
  MethodInvocationInContext(?ctx, ?invocation, ?taintTransferMethod),
  ParamToBaseTaintTransferMethod(?index, ?taintTransferMethod).

ParamTaintTransferredToBase(?param, ?ctx, ?base) <-
  TaintTransferMethodInvocationInContext(?ctx, ?index, ?invocation),
  ActualParam[?index, ?invocation] = ?param,
  MethodInvocation:Base[?invocation] = ?base,
  !AssignReturnValue[?invocation] = _.


MethodInvocationInContextInApplication(?ctx, ?invocation, ?method) <-
   Instruction:Method[?invocation] = ?fromMethod,
   MethodInvocationInContext(?ctx, ?invocation, ?method),
   ApplicationClass(Method:DeclaringType[?fromMethod]).

ParamTaintTransferredToRet(?type, ?ret, ?ctx, ?param) <-
   MethodInvocationInContextInApplication(?ctx, ?invocation, ?taintTransferMethod),
   ActualParam[0, ?invocation] = ?param,
   ParamToRetTaintTransferMethod(?taintTransferMethod),
   TypeForReturnValue(?type, ?ret, ?invocation).

TaintedValueTransferred(?value, ?type, DEFAULT_BREADCRUMB, ?newvalue),
VarPointsTo(?hctx, ?newvalue, ?ctx, ?to) <-
   ParamTaintTransferredToRet(?type, ?to, ?ctx, ?from),
   TaintedVarPointsTo(?value, ?ctx, ?from),
   ImmutableHContext(?hctx).



#ifdef INFORMATION_FLOW_HIGH_SOUNDNESS
ValueTaintedFromValue(?newvalue, ?baseObj) <- 
   BaseValueTaintedFromParamValue(?newvalue, ?baseObj),
   ?newvalue != ?baseObj.

VarPointsTo(?hctx, ?newvalue, ?ctx, ?var)
 <- 
   VarPointsToOpt(?ctx, ?var, ?existingvalue),
   ValueTaintedFromValue(?newvalue, ?existingvalue),
   ImmutableHContext(?hctx).

XYZ(?existingvalue, ?ctx, ?param) <-
  ParamTaintTransferredToBase(?param, ?ctx, ?base),
  VarPointsTo(_, ?existingvalue, ?ctx, ?base).


TaintedValueTransferred(?value, ?type, DEFAULT_BREADCRUMB, ?newvalue),
BaseValueTaintedFromParamValue(?newvalue, ?existingvalue) <-
  Value:Type[?existingvalue] = ?type,
  TaintedVarPointsTo(?value, ?ctx, ?param),
  XYZ(?existingvalue, ?ctx, ?param).

#else
XYZ2(?type, ?heap, ?ctx, ?param) <-
  ParamTaintTransferredToBase(?param, ?ctx, ?base),
  VarPointsTo(_, ?value, ?ctx, ?base),
  Value:Heap[?value] = ?heap,
  Value:Type[?value] = ?type.

TaintedValueTransferred(?value, ?type, DEFAULT_BREADCRUMB, ?newvalue),
VarPointsTo(?hctx, ?newvalue, ?ctx, ?var) <-
   XYZ2(?type, ?oldvalue, ?ctx, ?from),
   TaintedVarPointsTo(?value, ?ctx, ?from),
   AssignNormalHeapAllocation(?oldvalue, ?var, _),
   ImmutableHContext(?hctx).
#endif

/**
 * Sanitization TODO
 */

MethodInvocationInContext(?ctx, ?invocation, ?tomethod) -> Context(?ctx), MethodInvocation(?invocation), Method(?tomethod).

// Option 1
#ifdef INFORMATION_FLOW_HIGH_SOUNDNESS
MethodInvocationInMethod(?tomethod, ?invocation, ?inmethod) <-
   AnyMethodInvocationOpt(?tomethod, ?invocation),
   Instruction:Method[?invocation] = ?inmethod.

MethodInvocationInContext(?ctx, ?invocation, ?tomethod) <-
   MethodInvocationInMethod(?tomethod, ?invocation, ?inmethod),
   ReachableContext(?ctx, ?inmethod).


#else
// Option 2
MethodInvocationInContext(?ctx, ?invocation, ?tomethod) <-
   CallGraphEdge(?ctx, ?invocation, _, ?tomethod).

#endif
 
/**
 * Sinks and leaks
 */
LeakingSinkVariable(?label, ?invocation, ?ctx, ?var) <-
  MethodInvocationInContext(?ctx, ?invocation, ?tomethod),
  LeakingSinkMethodArg(?label, ?index, ?tomethod),
  ActualParam[?index, ?invocation] = ?var.

LeakingTaintedInformation(?sourceLabel, ?destLabel, ?ctx, ?invocation, ?source) <-
  TaintedVarPointsTo(?value, ?ctx, ?var),
  LeakingSinkVariable(?destLabel, ?invocation, ?ctx, ?var),
  SourceFromTaintedValue[?value] = ?source,
  LabelFromSource(?source, ?sourceLabel).

/**
 * Special Heap allocation on function call
 */
TaintedValueIntroduced(?declaringType, ?invocation, ?type, ?label, ?value),
VarPointsTo(?hctx, ?value, ?ctx, ?to) <-
   CallTaintingMethod(?label, ?ctx, ?invocation),
   ImmutableHContext(?hctx),
   TypeForReturnValue(?type, ?to, ?invocation),
   Method:DeclaringType[Instruction:Method[?invocation]] = ?declaringType.


TaintedVarPointsTo(?value, ?ctx, ?var) <-
  VarPointsToOpt(?ctx, ?var, ?value),
  TaintedValue(?value).

VarPointsToOpt(?ctx, ?var, ?value) <-
  VarPointsTo(_, ?value, ?ctx, ?var).

Foo(?value),
TaintedValue(?value),
TaintedValue(?basevalue) <-
  (TaintedValue(?value) ; TaintedValue(?basevalue)),
  InstanceFieldPointsTo(_, ?value, "<java.lang.String: char[] value>", _, ?basevalue).
